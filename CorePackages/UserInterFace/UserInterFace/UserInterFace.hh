#ifndef UTIL_USERINTERFACE_HH
#define UTIL_USERINTERFACE_HH
#include <iostream>
#include <string>
#include <vector>

class UserInterFace {
 public:
  UserInterFace();
  UserInterFace(int, char**);
  ~UserInterFace();
  void SetCondition();
  std::vector<std::string>getCommandList(){return m_commandList;}
  std::vector<std::string>getInputList(){return m_inputList;}
  std::string getgrPath(){return m_grPath;}
  int getgrVersion(){return m_grVersion;}
  std::string getJobType(){return m_jobtype;}
  std::string getTreename(){return m_treename;}
  std::string getTreename2(){return m_treename2;}
  std::string getPDFsysInput(){return m_pdfsysinput;}
  std::string getOutputFilename(){return m_output;}
  std::string getOutputTreename(){return m_outputtree;}
  std::string getSkimOutputFilename(){return m_skimoutput;}
  std::string getInputFileType(){return m_filetype;}
  std::string getCutFilename(){return m_cutfilename;}
  int getSample(){return m_sample;}
  int getTotalEvent(){return m_totalevent;}
  int getDecayMode(){return m_decaymode;}
  int getLepToTau(){return m_leptotau;}
  int getAntiele(){return m_antiele;}
  int getNoniso(){return m_noniso;}
  int getPileup(){return m_pileup;}
  int getLooseTau(){return m_loosetau;}
  int get7to8TeV(){return m_7to8tev;}
  double getMuScale(){return m_muscale;}
  int getJetele(){return m_jetele;}
  int getJESTESsys(){return m_jestessys;}
  int getJESsys(){return m_jessys;}
  int getTESPUsys(){return m_tespusys;}
  int getJESPUsys(){return m_jespusys;}
  int getCLsys(){return m_clsys;}
  int getPUsys(){return m_pusys;}
  int getEESsys(){return m_eessys;}
  int getEFFsys(){return m_effsys;}
  int getTrigger(){return m_trigger;}
  int getStartEvent(){return m_from_evt;}
  int getEndEvent(){return m_to_evt;}
  unsigned nCmd() const { return m_commandList.size(); }
  
 private:
  void addCommand(const std::string &str);
  const std::string& getCmdString(void);
  void doCmd(std::vector<std::string> & strs);
  void PrintHelp();
  std::string m_buffer;
  std::vector<std::string> m_commandList;
  std::vector<std::string> m_inputList;
  std::string m_grPath;
  int m_grVersion;
  std::string m_cutfilename;
  std::string m_jobtype;
  std::string m_treename;
  std::string m_treename2;
  std::string m_pdfsysinput;
  std::string m_output;
  std::string m_skimoutput;
  std::string m_outputtree;
  std::string m_filetype;
  int m_sample;
  int m_totalevent;
  int m_decaymode;
  int m_leptotau;
  int m_pileup;
  int m_loosetau;
  int m_7to8tev;
  double m_muscale;
  int m_antiele;
  int m_noniso;
  int m_jestessys;
  int m_jessys;
  int m_jespusys;
  int m_tespusys;
  int m_clsys;
  int m_pusys;
  int m_eessys;
  int m_effsys;
  int m_jetele;
  int m_trigger;
  int m_from_evt;
  int m_to_evt;

  
};

#endif // UTIL_USERINTERFACE_HH
