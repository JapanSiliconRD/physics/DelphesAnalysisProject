#ifndef __EVENTSELECTIONTOOLS_H__
#define __EVENTSELECTIONTOOLS_H__

#include <iostream>
#include <iomanip>
#include <vector>
#include "TH1D.h"

#include "SelectionBitDefinition.h"

class SelectionTools{
 public:
  //  SelectionTools(std::vector<TH1D*> &h_counter,std::vector<std::string> categoryname,bool islhChannel=true,bool total=true);
  SelectionTools(std::vector<TH1D*> &h_counter,std::vector<std::string> categoryname,int channeltype=1,bool total=true);
  ~SelectionTools(){}
  void SetName(std::string name){
    m_name=name;
  }
  void AddSelectionBit(int bit,int njets, double EventWeight=1.,bool TrigCateg=true,bool LepCateg=true);
  std::vector<TH1D *> GetSelectionHisto(){
    return m_h_counter;
  }
  void PrintAcceptanceTable(double weight=0.,bool doublesw=false);



 private:
  bool m_totalsw;
  //  bool m_islhChannel;
  int m_channeltype;
  std::string m_name;
  std::vector<TH1D*> m_h_counter;
  std::vector<std::string> m_categoryname;
};



#endif
