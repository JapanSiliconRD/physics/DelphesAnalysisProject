#ifndef _SELECTIONBITDEFINITION_H_
#define _SELECTIONBITDEFINITION_H_

#include <iostream>
#include <string>

namespace EventSelection {
  int EMUZBFRec_Trigger     = 0x00001;
  int EMUZBFRec_Muon        = 0x00002;
  int EMUZBFRec_Elec        = 0x00004;
  std::string EMUZBFRec_SelectionName[30]={"Initial            "
					   ,"Trigger            "
					   ,"AtLeastOneMuon     "  
					   ,"AtLeastOneElec     "};

  int EMUZBFHbb_Trigger     = 0x00001;
  int EMUZBFHbb_Muon        = 0x00002;
  int EMUZBFHbb_Elec        = 0x00004;
  int EMUZBFHbb_DiMuonVeto  = 0x00008;
  int EMUZBFHbb_DiElecVeto  = 0x00010;
  int EMUZBFHbb_Jets        = 0x00020;
  int EMUZBFHbb_BJets       = 0x00040;
  std::string EMUZBFHbb_SelectionName[30]={"Initial            "
					   ,"Trigger            "
					   ,"AtLeastOneMuon     "  
					   ,"AtLeastOneElec     "
					   ,"DiMuonVeto         "  
					   ,"DiElecVeto         "
					   ,"AtLeastTwoJets     "
					   ,"AtLeastTwoBJets    "};

  int EMUWBFHbb_Trigger     = 0x00001;
  int EMUWBFHbb_NoMuon      = 0x00002;
  int EMUWBFHbb_NoElec      = 0x00004;
  int EMUWBFHbb_Jets        = 0x00020;
  int EMUWBFHbb_BJets       = 0x00040;
  std::string EMUWBFHbb_SelectionName[30]={"Initial            "
					   ,"Trigger            "
					   ,"NoMuon             "  
					   ,"NoElec             "
					   ,"AtLeastTwoJets     "
					   ,"AtLeastTwoBJets    "};



  int TTHbb_Trigger    = 0x000001; 
  int TTHbb_Lepton     = 0x000002; 
  int TTHbb_DilVeto    = 0x000004; 
  std::string TTHbb_SelectionName[30]={"Initial            "
				       ,"Trigger            "
				       ,"AtLeastOneLepton   "  
				       ,"DiLeptonVeto       "};
  
  int TTLJets_Trigger    = 0x000001; 
  int TTLJets_Lepton     = 0x000002; 
  int TTLJets_DilVeto    = 0x000004; 
  std::string TTLJets_SelectionName[30]={"Initial            "
				       ,"Trigger            "
				       ,"AtLeastOneLepton   "  
				       ,"DiLeptonVeto       "};
  
  
  int TTDIL_Trigger    = 0x000001;
  int TTDIL_Lepton     = 0x000002;
  int TTDIL_TriLepVeto = 0x000004;
  std::string TTDIL_SelectionName[30]={"Initial            "
				       ,"Trigger            "
				       ,"AtLeastTwoLepton   "  
				       ,"TriLeptonVeto       "};
  


  ////  Htautau(lh) standard selection  /////////
  int Htaulh_Trigger             = 0x000001; // Cut 0
  int Htaulh_AtLeastOneLepton    = 0x000008; // Cut 1 
  int Htaulh_DiLeptonVeto        = 0x000010; // Cut 2 
  int Htaulh_OneHadronicTau      = 0x000020; // Cut 3 
  int Htaulh_MissingEt           = 0x000040; // Cut 4 
  int Htaulh_TransMassLepMet     = 0x000100; // Cut 6 
  int Htaulh_CollApprox          = 0x000080; // Cut 5 
  int Htaulh_AtLeastTwoJets      = 0x000200; // Cut 7 
  int Htaulh_FwdJetReq           = 0x000400; // Cut 8 
  int Htaulh_CentralityReq       = 0x000800; // Cut 9 
  int Htaulh_JetSeparation       = 0x001000; // Cut 10
  int Htaulh_DiJetMass           = 0x002000; // Cut 11
  int Htaulh_CentralJetVeto      = 0x004000; // Cut 12
  int Htaulh_MassWindow          = 0x008000; 
  //////////////////////////////////////////////
  std::string Htaulh_SelectionName[15]={"Initial          ",
					"Trigger          ",
					"AtLeastOneLepton ",
					"DiLeptonVeto     ",
					"OneHadronicTau   ",
					"MissingEt        ",
					"TransMassLepMet  ",
					"CollApprox       ",
					"AtLeastTwoJets   ",
					"FwdJetReq        ",
					"CentralityReq    ",
					"JetSeparation    ",
					"DiJetMass        ",
					"CentralJetVeto   ",
					"MassWindow       "};
  ////  Htautau(lh) standard selection  /////////
  int Htaull_Trigger             = 0x000001;
  int Htaull_AtLeastOneLepton    = 0x000008;
  int Htaull_DiLepton            = 0x000010;
  int Htaull_MissingEt           = 0x000040;
  int Htaull_CollApprox          = 0x000080;
  int Htaull_AtLeastTwoJets      = 0x000200;
  int Htaull_FwdJetReq           = 0x000400;
  int Htaull_CentralityReq       = 0x000800;
  int Htaull_BtagVeto            = 0x010000;
  int Htaull_JetSeparation       = 0x001000;
  int Htaull_DiJetMass           = 0x002000;
  int Htaull_CentralJetVeto      = 0x004000;
  int Htaull_MassWindow          = 0x008000;
  //////////////////////////////////////////////
  std::string Htaull_SelectionName[14]={"Initial          ",
					"Trigger          ",
					"AtLeastOneLepton ",
					"DiLepton         ",
					"MissingEt        ",
					"CollApprox       ",
					"AtLeastTwoJets   ",
					"FwdJetReq        ",
					"CentralityReq    ",
					"BtagVeto         ",
					"JetSeparation    ",
					"DiJetMass        ",
					"CentralJetVeto   ",
					"MassWindow       "};


  //// NEW !!!  Htautau(lh) standard selection for data MC comparison !!  /////////
  int LJets_GoodRunList         = 0x000001; // Cut 0
  int LJets_Trigger             = 0x000002; // Cut 0
  int LJets_Cleaning0           = 0x000004; // Cut 0
  int LJets_Cleaning1           = 0x000008; // Cut 1 
  int LJets_Cleaning2           = 0x000010; // Cut 1 
  int LJets_Cleaning3           = 0x000020; // Cut 1 
  int LJets_Cleaning4           = 0x000040; // Cut 1 
  int LJets_AtLeastOneLepton    = 0x000080; // Cut 1 
  int LJets_DiLeptonVeto        = 0x000100; // Cut 2 
  int LJets_OneHadronicTau      = 0x000200; // Cut 3 
  int LJets_MissingEt           = 0x000400; // Cut 4 
  int LJets_TransMassLepMet     = 0x000800; // Cut 6 
  int LJets_SumDeltaPhi         = 0x001000; 
  int LJets_MMCSucceeded        = 0x002000; 
  int LJets_ADDITIONALCUT       = 0x004000; 
  int LJets_CollApprox          = 0x008000; // Cut 5 
  int LJets_AtLeastTwoJets      = 0x010000; // Cut 7 
  int LJets_FwdJetReq           = 0x020000; // Cut 8 
  int LJets_CentralityReq       = 0x040000; // Cut 9 
  int LJets_JetSeparation       = 0x080000; // Cut 10
  int LJets_DiJetMass           = 0x100000; // Cut 11
  int LJets_CentralJetVeto      = 0x200000; // Cut 12
  int LJets_MassWindow          = 0x400000; 
  //  int LJets_JetSeparationT      = 0x800000; // Cut 10
  //  int LJets_DiJetMassT          = 0x1000000; // Cut 11



  //////////////////////////////////////////////
  std::string LJets_SelectionName[24]={"Initial          ",
					"GoodRunList      ",
					"Trigger          ",
					"Vertex           ",
					"Jet Cleaning     ",
					"(not assigned)   ",
					"(not assigned)   ",
					"(not assigned)   ",
					"AtLeastOneLepton ",
					"DiLeptonVeto     ",
					"OneHadronicTau   ",
					"MissingEt        ",
					"TransMassLepMet  ",
					"SumDeltaPhi      ",
					"MMCSucceeded     ",
					"(not assigned)   ",
					"CollApprox       ",
					"AtLeastTwoJets   ",
					"FwdJetReq        ",
					"CentralityReq    ",
					"JetSeparation    ",
					"DiJetMass        ",
					"CentralJetVeto   ",
					"MassWindow       "};



  //// NEW !!!  Htautau(lh) standard selection for data MC comparison !!  /////////
  int VH_GoodRunList         = 0x000001; // Cut 0
  int VH_Trigger             = 0x000002; // Cut 0
  int VH_Cleaning0           = 0x000004; // Cut 0
  int VH_Cleaning1           = 0x000008; // Cut 1 
  int VH_Cleaning2           = 0x000010; // Cut 1 
  int VH_Cleaning3           = 0x000020; // Cut 1 
  int VH_Cleaning4           = 0x000040; // Cut 1 
  int VH_AtLeastOneLepton    = 0x000080; // Cut 1 
  int VH_DiLepton            = 0x000100; // Cut 2 
  int VH_OSDiLepton          = 0x000200; // Cut 3 
  int VH_SSDiLepton          = 0x000400; // Cut 4 
  int VH_TriLepton           = 0x000800; // Cut 6 
  int VH_DilORTri            = 0x001000; 
  int VH_OneHadronicTau      = 0x002000; 
  int VH_AbsSumCharge        = 0x004000; 
  int VH_ADDITIONALCUT1     = 0x008000; // Cut 5 
  int VH_ADDITIONALCUT2     = 0x010000; // Cut 7 
  int VH_ADDITIONALCUT3     = 0x020000; // Cut 8 
  int VH_ADDITIONALCUT4     = 0x040000; // Cut 9 
  int VH_ADDITIONALCUT5     = 0x080000; // Cut 10
  int VH_ADDITIONALCUT6     = 0x100000; // Cut 11
  int VH_ADDITIONALCUT7     = 0x200000; // Cut 12
  int VH_ADDITIONALCUT8     = 0x400000; 
  //  int VH_JetSeparationT      = 0x800000; // Cut 10
  //  int VH_DiJetMassT          = 0x1000000; // Cut 11



  //////////////////////////////////////////////
  std::string VH_SelectionName[24]={"Initial          ",
				    "GoodRunList      ",
				    "Trigger          ",
				    "Vertex           ",
				    "Jet Cleaning     ",
				    "(not assigned)   ",
				    "(not assigned)   ",
				    "(not assigned)   ",
				    "AtLeastOneLepton ",
				    "DiLepton         ",
				    "OSDiLepton       ",
				    "SSDiLepton       ",
				    "TriLepton        ",
				    "DilORTri         ",
				    "OneHadronicTau   ",
				    "AbsSumCharge     ",
				    "ADDITIONALCUT1   ",
				    "ADDITIONALCUT2   ",
				    "ADDITIONALCUT3   ",
				    "ADDITIONALCUT4   ",
				    "ADDITIONALCUT5   ",
				    "ADDITIONALCUT6   ",
				    "ADDITIONALCUT7   ",
				    "ADDITIONALCUT8   "};
  



  //// NEW !!!  Htautau(lh) standard selection for data MC comparison !!  /////////
  int ZZ4l_GoodRunList         = 0x000001; // Cut 0
  int ZZ4l_Trigger             = 0x000002; // Cut 0
  int ZZ4l_Cleaning0           = 0x000004; // Cut 0
  int ZZ4l_Cleaning1           = 0x000008; // Cut 1 
  int ZZ4l_Cleaning2           = 0x000010; // Cut 1 
  int ZZ4l_Cleaning3           = 0x000020; // Cut 1 
  int ZZ4l_Cleaning4           = 0x000040; // Cut 1 
  int ZZ4l_AtLeastOneLepton    = 0x000080; // Cut 1 
  int ZZ4l_AtLeastTwoLepton    = 0x000100; // Cut 2 
  int ZZ4l_AtLeastOneMlleqZmass= 0x000200; // Cut 3 
  int ZZ4l_AtLeastThreeLepton  = 0x000400; // Cut 4 
  int ZZ4l_AtLeastFourLepton   = 0x000800; // Cut 6 
  int ZZ4l_AtLeastTwoMlleqZmass= 0x001000; // Cut 5 
  int ZZ4l_EEEE                = 0x002000; // Cut 7 
  int ZZ4l_MMMM                = 0x004000; // Cut 8 
  int ZZ4l_EEMM                = 0x008000; // Cut 9 
  int ZZ4l_EXTRACUT3           = 0x010000; // Cut 10
  int ZZ4l_EXTRACUT4           = 0x020000; // Cut 11
  int ZZ4l_EXTRACUT5           = 0x040000; // Cut 12
  int ZZ4l_EXTRACUT6           = 0x080000; 
  //////////////////////////////////////////////
  std::string ZZ4l_SelectionName[21]={"Initial               ",
				      "GoodRunList           ",
				      "Trigger               ",
				      "Vertex                ",
				      "Jet Cleaning          ",
				      "(not assigned)        ",
				      "(not assigned)        ",
				      "(not assigned)        ",
				      "AtLeastOneLepton      ",
				      "AtLeastTwoLepton      ",
				      "AtLeastOneMlleqZmass  ",
				      "AtLeastThreeLepton    ",
				      "AtLeastFourLepton     ",
				      "AtLeastTwoMlleqZmass  ",
				      "EEEE                  ",
				      "MMMM                  ",
				      "EEMM                  ",
				      "EXTRACUT3             ",
				      "EXTRACUT4             ",
				      "EXTRACUT5             ",
				      "EXTRACUT6             "};




}

namespace ElectronCut{
  //  object                 failed bit         selction
  int  CutAuthor          =  0x000001;	//   1 or 3    
  int  CutPt              =  0x000002;	//   >15       
  int  CutEta             =  0x000004;	//   <2.7      
  int  CutIsolation       =  0x000008;	//   <0.1      
  
  int  CutType            =  0x001000;	//     0       
}

namespace MuonCut{
  int  CutAuthor          =  0x000001;
  int  CutPt              =  0x000002;
  int  CutEta             =  0x000004;
  int  CutIsolation       =  0x000008;
  int  CutBestMat         =  0x000010;
  int  CutMatChi2         =  0x000020;
  int  CutFitChi2         =  0x000040;

  int  CutType            =  0x001000;
}

namespace TauJetCut{
  int  CutAuthor          =  0x000001;
  int  CutEt              =  0x000002;
  int  CutEta             =  0x000004;
  int  CutNTrack          =  0x000008;
  int  CutCharge          =  0x000010;
  int  CutLikelihood      =  0x000020;
  int  CutElTauLikelihood =  0x000040;
  int  CutVetoFlag        =  0x000080;
  int  CutType            =  0x001000;
}

namespace JetCut{
  int  CutEt              =  0x000001;
  int  CutEta             =  0x000002;
}

namespace LeptonCategory{
  int LepCateg_SingleElectron   = 0x00001;
  int LepCateg_SingleMuon       = 0x00002;
  int LepCateg_DiElectron       = 0x00004;
  int LepCateg_DiMuon           = 0x00008;
  int LepCateg_EleMuo           = 0x00010;
}

#endif
