cmake_minimum_required(VERSION 3.1)

project(SelectionTool LANGUAGES CXX)

# Finding the ROOT package
find_package(ROOT 6.16 CONFIG REQUIRED)
# Sets up global settings
include("${ROOT_USE_FILE}")
include_directories(${PROJECT_NAME})

# Adding an executable program and linking to needed ROOT libraries
file( GLOB SRCS src/*.cc src/*.cxx )
add_library(${PROJECT_NAME} SHARED ${SRCS})


set(TARGET_INSTALL_AREA ${PROJECT_SOURCE_DIR})
set(SHARED_INSTALL_AREA ${PROJECT_SOURCE_DIR}/installed/)
add_compile_options(-std=c++11)


INSTALL(TARGETS ${PROJECT_NAME}
        EXPORT ${PROJECT_NAME}
        RUNTIME DESTINATION ${PROJECT_SOURCE_DIR}/bin
        LIBRARY DESTINATION ${PROJECT_SOURCE_DIR}/lib
        ARCHIVE DESTINATION ${PROJECT_SOURCE_DIR}/lib)
