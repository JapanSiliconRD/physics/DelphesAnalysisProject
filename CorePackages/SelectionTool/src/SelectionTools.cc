#include "SelectionBitDefinition.h"
#include "SelectionTools.h"

SelectionTools::SelectionTools(std::vector<TH1D*> &h_counter,std::vector<std::string> categoryname,int channeltype,bool total){
  m_name="no name";
  m_totalsw=total;
  //  m_islhChannel=islhChannel;
  m_channeltype=channeltype;
  //  m_h_counter = h_counter;
  for(unsigned int ii=0;ii<categoryname.size();ii++){
    m_h_counter.push_back(h_counter[ii]);
    m_categoryname.push_back(categoryname[ii]);
  }
}

void SelectionTools::AddSelectionBit(int bit,int njets, double EventWeight,bool TrigCateg,bool LepCateg){
  if(m_channeltype==1){
    if(m_totalsw){
      m_h_counter[njets]->Fill(0.,EventWeight);
      if(!(bit&EventSelection::Htaulh_Trigger         )) m_h_counter[njets]->Fill(1,EventWeight);  else return;
    }
    if(!(bit&EventSelection::Htaulh_AtLeastOneLepton)) m_h_counter[njets]->Fill(2,EventWeight);  else return;
    if(!(bit&EventSelection::Htaulh_DiLeptonVeto    )) m_h_counter[njets]->Fill(3,EventWeight);  else return;
    if(!(bit&EventSelection::Htaulh_OneHadronicTau  )) m_h_counter[njets]->Fill(4,EventWeight);  else return;
    if(!(bit&EventSelection::Htaulh_MissingEt       )) m_h_counter[njets]->Fill(5,EventWeight);  else return;
    if(!(bit&EventSelection::Htaulh_TransMassLepMet )) m_h_counter[njets]->Fill(6,EventWeight);  else return;
    if(!(bit&EventSelection::Htaulh_CollApprox      )) m_h_counter[njets]->Fill(7,EventWeight);  else return;
    if(!(bit&EventSelection::Htaulh_AtLeastTwoJets  )) m_h_counter[njets]->Fill(8,EventWeight);  else return;
    if(!(bit&EventSelection::Htaulh_FwdJetReq       )) m_h_counter[njets]->Fill(9,EventWeight);  else return;
    if(!(bit&EventSelection::Htaulh_CentralityReq   )) m_h_counter[njets]->Fill(10,EventWeight); else return;
    if(!(bit&EventSelection::Htaulh_JetSeparation   )) m_h_counter[njets]->Fill(11,EventWeight); else return;
    if(!(bit&EventSelection::Htaulh_DiJetMass       )) m_h_counter[njets]->Fill(12,EventWeight); else return;
    if(!(bit&EventSelection::Htaulh_CentralJetVeto  )) m_h_counter[njets]->Fill(13,EventWeight); else return;
    if(!(bit&EventSelection::Htaulh_MassWindow      )) m_h_counter[njets]->Fill(14,EventWeight); else return;
  } else if(m_channeltype==0){
    if(m_totalsw){
      m_h_counter[njets]->Fill(0.,EventWeight);
      if(!(bit&EventSelection::Htaull_Trigger         )) m_h_counter[njets]->Fill(1,EventWeight);  else return;
    }
    if(!(bit&EventSelection::Htaull_AtLeastOneLepton)) m_h_counter[njets]->Fill(2,EventWeight);  else return;
    if(!(bit&EventSelection::Htaull_DiLepton        )) m_h_counter[njets]->Fill(3,EventWeight);  else return;
    if(!(bit&EventSelection::Htaull_MissingEt       )) m_h_counter[njets]->Fill(4,EventWeight);  else return;
    if(!(bit&EventSelection::Htaull_CollApprox      )) m_h_counter[njets]->Fill(5,EventWeight);  else return;
    if(!(bit&EventSelection::Htaull_AtLeastTwoJets  )) m_h_counter[njets]->Fill(6,EventWeight);  else return;
    if(!(bit&EventSelection::Htaull_FwdJetReq       )) m_h_counter[njets]->Fill(7,EventWeight);  else return;
    if(!(bit&EventSelection::Htaull_CentralityReq   )) m_h_counter[njets]->Fill(8,EventWeight); else return;
    if(!(bit&EventSelection::Htaull_BtagVeto        )) m_h_counter[njets]->Fill(9,EventWeight); else return;
    if(!(bit&EventSelection::Htaull_JetSeparation   )) m_h_counter[njets]->Fill(10,EventWeight); else return;
    if(!(bit&EventSelection::Htaull_DiJetMass       )) m_h_counter[njets]->Fill(11,EventWeight); else return;
    if(!(bit&EventSelection::Htaull_CentralJetVeto  )) m_h_counter[njets]->Fill(12,EventWeight); else return;
    if(!(bit&EventSelection::Htaull_MassWindow      )) m_h_counter[njets]->Fill(13,EventWeight); else return;
  }else if(m_channeltype==2){
    if(m_totalsw){
      m_h_counter[njets]->Fill(0.,EventWeight);
    }
    if(!(bit&EventSelection::LJets_GoodRunList     )) m_h_counter[njets]->Fill(1,EventWeight);  else return;
    if(TrigCateg){
      if(!(bit&EventSelection::LJets_Trigger         )) m_h_counter[njets]->Fill(2,EventWeight);  else return;
      if(!(bit&EventSelection::LJets_Cleaning0       )) m_h_counter[njets]->Fill(3,EventWeight);  else return;
      if(!(bit&EventSelection::LJets_Cleaning1       )) m_h_counter[njets]->Fill(4,EventWeight);  else return;
      if(!(bit&EventSelection::LJets_Cleaning2       )) m_h_counter[njets]->Fill(5,EventWeight);  else return;
      if(!(bit&EventSelection::LJets_Cleaning3       )) m_h_counter[njets]->Fill(6,EventWeight);  else return;
      if(!(bit&EventSelection::LJets_Cleaning4       )) m_h_counter[njets]->Fill(7,EventWeight);  else return;
    }
    if(LepCateg){
      if(!(bit&EventSelection::LJets_AtLeastOneLepton)) m_h_counter[njets]->Fill(8,EventWeight);  else return;
      if(!(bit&EventSelection::LJets_DiLeptonVeto    )) m_h_counter[njets]->Fill(9,EventWeight);  else return;
      if(!(bit&EventSelection::LJets_OneHadronicTau  )) m_h_counter[njets]->Fill(10,EventWeight); else return;
      if(!(bit&EventSelection::LJets_MissingEt       )) m_h_counter[njets]->Fill(11,EventWeight); else return;
      if(!(bit&EventSelection::LJets_TransMassLepMet )) m_h_counter[njets]->Fill(12,EventWeight); else return;
      if(!(bit&EventSelection::LJets_SumDeltaPhi     )) m_h_counter[njets]->Fill(13,EventWeight); else return;
      if(!(bit&EventSelection::LJets_MMCSucceeded    )) m_h_counter[njets]->Fill(14,EventWeight); else return;
      if(!(bit&EventSelection::LJets_ADDITIONALCUT   )) m_h_counter[njets]->Fill(15,EventWeight); else return;

      //      if(!(bit&EventSelection::LJets_CollApprox      )) m_h_counter[njets]->Fill(16,EventWeight); else return;
      if(1) m_h_counter[njets]->Fill(16,EventWeight); else return;
      if(!(bit&EventSelection::LJets_AtLeastTwoJets  )) m_h_counter[njets]->Fill(17,EventWeight); else return;
      if(!(bit&EventSelection::LJets_FwdJetReq       )) m_h_counter[njets]->Fill(18,EventWeight); else return;
      if(!(bit&EventSelection::LJets_CentralityReq   )) m_h_counter[njets]->Fill(19,EventWeight); else return;
      if(!(bit&EventSelection::LJets_JetSeparation   )) m_h_counter[njets]->Fill(20,EventWeight); else return;
      if(!(bit&EventSelection::LJets_DiJetMass       )) m_h_counter[njets]->Fill(21,EventWeight); else return;
      if(!(bit&EventSelection::LJets_CentralJetVeto  )) m_h_counter[njets]->Fill(22,EventWeight); else return;
      if(!(bit&EventSelection::LJets_MassWindow      )) m_h_counter[njets]->Fill(23,EventWeight); else return;
    }
  }else if(m_channeltype==3){
    if(m_totalsw){
      m_h_counter[njets]->Fill(0.,EventWeight);
    }
    if(!(bit&EventSelection::ZZ4l_GoodRunList     )) m_h_counter[njets]->Fill(1,EventWeight);  else return;
    if(TrigCateg){
      if(!(bit&EventSelection::ZZ4l_Trigger         )) m_h_counter[njets]->Fill(2,EventWeight);  else return;
      if(!(bit&EventSelection::ZZ4l_Cleaning0       )) m_h_counter[njets]->Fill(3,EventWeight);  else return;
      if(!(bit&EventSelection::ZZ4l_Cleaning1       )) m_h_counter[njets]->Fill(4,EventWeight);  else return;
      if(!(bit&EventSelection::ZZ4l_Cleaning2       )) m_h_counter[njets]->Fill(5,EventWeight);  else return;
      if(!(bit&EventSelection::ZZ4l_Cleaning3       )) m_h_counter[njets]->Fill(6,EventWeight);  else return;
      if(!(bit&EventSelection::ZZ4l_Cleaning4       )) m_h_counter[njets]->Fill(7,EventWeight);  else return;
    }
    if(LepCateg){
      if(!(bit&EventSelection::ZZ4l_AtLeastOneLepton    )) m_h_counter[njets]->Fill(8,EventWeight);  else return;
      if(!(bit&EventSelection::ZZ4l_AtLeastTwoLepton    )) m_h_counter[njets]->Fill(9,EventWeight);  else return;
      if(!(bit&EventSelection::ZZ4l_AtLeastOneMlleqZmass)) m_h_counter[njets]->Fill(10,EventWeight); else return;
      if(!(bit&EventSelection::ZZ4l_AtLeastThreeLepton  )) m_h_counter[njets]->Fill(11,EventWeight); else return;
      if(!(bit&EventSelection::ZZ4l_AtLeastFourLepton   )) m_h_counter[njets]->Fill(12,EventWeight); else return;
      if(!(bit&EventSelection::ZZ4l_AtLeastTwoMlleqZmass)) m_h_counter[njets]->Fill(13,EventWeight); else return;
      if(!(bit&EventSelection::ZZ4l_EEEE                )) m_h_counter[njets]->Fill(14,EventWeight);// else return;
      if(!(bit&EventSelection::ZZ4l_MMMM                )) m_h_counter[njets]->Fill(15,EventWeight);// else return;
      if(!(bit&EventSelection::ZZ4l_EEMM                )) m_h_counter[njets]->Fill(16,EventWeight);// else return;
      if(!(bit&EventSelection::ZZ4l_EXTRACUT3           )) m_h_counter[njets]->Fill(17,EventWeight); else return;
      if(!(bit&EventSelection::ZZ4l_EXTRACUT4           )) m_h_counter[njets]->Fill(18,EventWeight); else return;
      if(!(bit&EventSelection::ZZ4l_EXTRACUT5           )) m_h_counter[njets]->Fill(19,EventWeight); else return;
      if(!(bit&EventSelection::ZZ4l_EXTRACUT6           )) m_h_counter[njets]->Fill(20,EventWeight); else return;
    }
  }else if(m_channeltype==4){
    if(m_totalsw){
      m_h_counter[njets]->Fill(0.,EventWeight);
    }
    if(!(bit&EventSelection::VH_GoodRunList     )) m_h_counter[njets]->Fill(1,EventWeight);  else return;
    if(TrigCateg){
      if(!(bit&EventSelection::VH_Trigger         )) m_h_counter[njets]->Fill(2,EventWeight);  else return;
      if(!(bit&EventSelection::VH_Cleaning0       )) m_h_counter[njets]->Fill(3,EventWeight);  else return;
      if(!(bit&EventSelection::VH_Cleaning1       )) m_h_counter[njets]->Fill(4,EventWeight);  else return;
      if(!(bit&EventSelection::VH_Cleaning2       )) m_h_counter[njets]->Fill(5,EventWeight);  else return;
      if(!(bit&EventSelection::VH_Cleaning3       )) m_h_counter[njets]->Fill(6,EventWeight);  else return;
      if(!(bit&EventSelection::VH_Cleaning4       )) m_h_counter[njets]->Fill(7,EventWeight);  else return;
      if(!(bit&EventSelection::VH_AtLeastOneLepton   )) m_h_counter[njets]->Fill(8,EventWeight);  else return;
      if(!(bit&EventSelection::VH_DiLepton           )) m_h_counter[njets]->Fill(9,EventWeight);  else return;
      if((bit&EventSelection::VH_OSDiLepton         )) m_h_counter[njets]->Fill(10,EventWeight);// else return;
      if((bit&EventSelection::VH_SSDiLepton         )) m_h_counter[njets]->Fill(11,EventWeight);// else return;
    }
    if(LepCateg){
      if((bit&EventSelection::VH_TriLepton          )) m_h_counter[njets]->Fill(12,EventWeight);// else return;
      if(!(bit&EventSelection::VH_DilORTri          )) m_h_counter[njets]->Fill(13,EventWeight); else return;
      if(!(bit&EventSelection::VH_OneHadronicTau    )) m_h_counter[njets]->Fill(14,EventWeight); else return;
      if(!(bit&EventSelection::VH_AbsSumCharge       )) m_h_counter[njets]->Fill(15,EventWeight); else return;
      if(!(bit&EventSelection::VH_ADDITIONALCUT1     )) m_h_counter[njets]->Fill(16,EventWeight); else return;
      if(!(bit&EventSelection::VH_ADDITIONALCUT2     )) m_h_counter[njets]->Fill(17,EventWeight); else return;
      if(!(bit&EventSelection::VH_ADDITIONALCUT3     )) m_h_counter[njets]->Fill(18,EventWeight); else return;
      if(!(bit&EventSelection::VH_ADDITIONALCUT4     )) m_h_counter[njets]->Fill(19,EventWeight); else return;
      if(!(bit&EventSelection::VH_ADDITIONALCUT5     )) m_h_counter[njets]->Fill(20,EventWeight); else return;
      if(!(bit&EventSelection::VH_ADDITIONALCUT6     )) m_h_counter[njets]->Fill(21,EventWeight); else return;
      if(!(bit&EventSelection::VH_ADDITIONALCUT7     )) m_h_counter[njets]->Fill(22,EventWeight); else return;
      if(!(bit&EventSelection::VH_ADDITIONALCUT8     )) m_h_counter[njets]->Fill(23,EventWeight); else return;
    }
  }else if(m_channeltype==10){ // 
    if(m_totalsw){
      m_h_counter[njets]->Fill(0.,EventWeight);
      if(!(bit&EventSelection::EMUZBFHbb_Trigger         )) m_h_counter[njets]->Fill(1,EventWeight);  else return;
    }
    if(!(bit&EventSelection::EMUZBFHbb_Muon            )) m_h_counter[njets]->Fill(2,EventWeight);  else return;
    if(!(bit&EventSelection::EMUZBFHbb_Elec            )) m_h_counter[njets]->Fill(3,EventWeight);  else return;
    if(!(bit&EventSelection::EMUZBFHbb_DiMuonVeto      )) m_h_counter[njets]->Fill(4,EventWeight);  else return;
    if(!(bit&EventSelection::EMUZBFHbb_DiElecVeto      )) m_h_counter[njets]->Fill(5,EventWeight);  else return;
    if(!(bit&EventSelection::EMUZBFHbb_Jets            )) m_h_counter[njets]->Fill(6,EventWeight);  else return;
    if(!(bit&EventSelection::EMUZBFHbb_BJets           )) m_h_counter[njets]->Fill(7,EventWeight);  else return;
  }else if(m_channeltype==11){ // 
    if(m_totalsw){
      m_h_counter[njets]->Fill(0.,EventWeight);
      if(!(bit&EventSelection::EMUWBFHbb_Trigger        )) m_h_counter[njets]->Fill(1,EventWeight);  else return;
    }
    if(!(bit&EventSelection::EMUWBFHbb_NoMuon           )) m_h_counter[njets]->Fill(2,EventWeight);  else return;
    if(!(bit&EventSelection::EMUWBFHbb_NoElec           )) m_h_counter[njets]->Fill(3,EventWeight);  else return;
    if(!(bit&EventSelection::EMUWBFHbb_Jets             )) m_h_counter[njets]->Fill(4,EventWeight);  else return;
    if(!(bit&EventSelection::EMUWBFHbb_BJets            )) m_h_counter[njets]->Fill(5,EventWeight);  else return;
  }
  return;
}

void SelectionTools::PrintAcceptanceTable(double weight,bool doublesw){
  double denominator=1;
  if(m_totalsw&&weight<0)denominator=m_h_counter[m_categoryname.size()-1]->GetBinContent(1)/100.;
  if(weight>0)denominator=1./weight;
  std::cerr << "==============================="
	    << m_name
	    << "===============================" << std::endl;
  std::cerr << std::setw(20) << std::setprecision(10) << "" ;
  
  for(unsigned int injetc=0;injetc<m_categoryname.size();injetc++){
    std::cerr << " & " << std::setw(11) << m_categoryname[injetc] << " " ;
  }
  std::cerr << "\\\\ "<< std::endl;
  if(m_channeltype==1){
    for(int icut=1; icut<16;icut++){
      std::cerr << std::setw(2) << icut << " " << EventSelection::Htaulh_SelectionName[icut-1];
      for(unsigned int injetc=0;injetc<m_categoryname.size();injetc++){
	if(!m_totalsw && icut<3){
	  std::cerr << " & " << std::setw(11) 
		    << "--" << " ";
	}else{
	  if(doublesw){
	    std::cerr << " & " << std::setw(11) 
		      << (double)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }else{
	    std::cerr << " & " << std::setw(11) 
		      << (long)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }
	}
      }
      std::cerr << "\\\\" << std::endl;
    }
  }else if(m_channeltype==0){
    for(int icut=1; icut<15;icut++){
      std::cerr << std::setw(2) << icut << " " << EventSelection::Htaull_SelectionName[icut-1] ;
      for(unsigned int injetc=0;injetc<m_categoryname.size();injetc++){
	if(!m_totalsw && icut<3){
	  std::cerr << " & " << std::setw(11) 
		    << "--" << " ";
	}else{
	  if(doublesw){
	    std::cerr << " & " << std::setw(11) 
		      << (double)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }else{
	    std::cerr << " & " << std::setw(11) 
		      << (long)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }
	}
      }
      std::cerr << "\\\\" << std::endl;
    }
  }else if(m_channeltype==2){
    for(int icut=1; icut<25;icut++){
      std::cerr << std::setw(2) << icut << " " << EventSelection::LJets_SelectionName[icut-1] ;
      for(unsigned int injetc=0;injetc<m_categoryname.size();injetc++){
	if(!m_totalsw && icut<3){
	  std::cerr << " & " << std::setw(11) 
		    << "--" << " ";
	}else{
	  if(doublesw){
	    std::cerr << " & " << std::setw(11) 
		      << (double)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }else{
	    std::cerr << " & " << std::setw(11) 
		      << (long)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }
	}
      }
      std::cerr << "\\\\" << std::endl;
    }
  }else if(m_channeltype==3){
    for(int icut=1; icut<22;icut++){
      std::cerr << std::setw(2) << icut << " " << EventSelection::ZZ4l_SelectionName[icut-1] ;
      for(unsigned int injetc=0;injetc<m_categoryname.size();injetc++){
	if(!m_totalsw && icut<3){
	  std::cerr << " & " << std::setw(11) 
		    << "--" << " ";
	}else{
	  if(doublesw){
	    std::cerr << " & " << std::setw(11) 
		      << (double)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }else{
	    std::cerr << " & " << std::setw(11) 
		      << (long)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }
	}
      }
      std::cerr << "\\\\" << std::endl;
    }
  }else if(m_channeltype==4){
    for(int icut=1; icut<25;icut++){
      std::cerr << std::setw(2) << icut << " " << EventSelection::VH_SelectionName[icut-1] ;
      for(unsigned int injetc=0;injetc<m_categoryname.size();injetc++){
	if(!m_totalsw && icut<3){
	  std::cerr << " & " << std::setw(11) 
		    << "--" << " ";
	}else{
	  if(doublesw){
	    std::cerr << " & " << std::setw(11) 
		      << (double)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }else{
	    std::cerr << " & " << std::setw(11) 
		      << (long)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }
	}
      }
      std::cerr << "\\\\" << std::endl;
    }
  }else if(m_channeltype==10){
    for(int icut=1; icut<9;icut++){
      std::cerr << std::setw(2) << icut << " " << EventSelection::EMUZBFHbb_SelectionName[icut-1] ;
      for(unsigned int injetc=0;injetc<m_categoryname.size();injetc++){
	if(!m_totalsw && icut<3){
	  std::cerr << " & " << std::setw(11) 
		    << "--" << " ";
	}else{
	  if(doublesw){
	    std::cerr << " & " << std::setw(11) 
		      << (double)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }else{
	    std::cerr << " & " << std::setw(11) 
		      << (long)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }
	}
      }
      std::cerr << "\\\\" << std::endl;
    }
  }else if(m_channeltype==11){
    for(int icut=1; icut<7;icut++){
      std::cerr << std::setw(2) << icut << " " << EventSelection::EMUWBFHbb_SelectionName[icut-1] ;
      for(unsigned int injetc=0;injetc<m_categoryname.size();injetc++){
	if(!m_totalsw && icut<3){
	  std::cerr << " & " << std::setw(11) 
		    << "--" << " ";
	}else{
	  if(doublesw){
	    std::cerr << " & " << std::setw(11) 
		      << (double)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }else{
	    std::cerr << " & " << std::setw(11) 
		      << (long)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }
	}
      }
      std::cerr << "\\\\" << std::endl;
    }
  }else if(m_channeltype==12){
    for(int icut=1; icut<5;icut++){
      std::cerr << std::setw(2) << icut << " " << EventSelection::EMUZBFRec_SelectionName[icut-1] ;
      for(unsigned int injetc=0;injetc<m_categoryname.size();injetc++){
	if(!m_totalsw && icut<3){
	  std::cerr << " & " << std::setw(11) 
		    << "--" << " ";
	}else{
	  if(doublesw){
	    std::cerr << " & " << std::setw(11) 
		      << (double)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }else{
	    std::cerr << " & " << std::setw(11) 
		      << (long)m_h_counter[injetc]->GetBinContent(icut)/denominator << " ";
	  }
	}
      }
      std::cerr << "\\\\" << std::endl;
    }
  }
}
