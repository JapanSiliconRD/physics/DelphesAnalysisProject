#include "FileRead.hh"


int FileRead::SetChain(TChain* chain, UserInterFace ui, std::string treename){
  if(ui.getInputList().size()==0){
    //    SetChain(chain,ui.getSample());
    std::cout << " please set input files" << std::endl;
  }
  for(unsigned int ii=0;ii<ui.getInputList().size();ii++){
    std::cout << "file: " 
	      << ui.getInputList()[ii].substr(ui.getInputList()[ii].rfind("/")+1)
	      << std::endl;
    //    int nfile = chain->AddFile(ui.getInputList()[ii].c_str(),-1);
    //    int nfile = chain->Add(ui.getInputList()[ii].c_str(),-1);
    std::cout <<ui.getInputList()[ii]+"/"+treename<< std::endl;
    int nfile = chain->Add((ui.getInputList()[ii]+"/"+treename).c_str(),-1);
    
    std::cout << "   ==> " << std::setw(7) << chain->GetEntries() 
	      << "  events (" <<  nfile<< ")"<< std::endl;
    //    if(ui.getEndEvent() && (chain->GetEntries()>ui.getEndEvent()))break;
    if(nfile==0){
      std::cout << ui.getInputList()[ii].c_str() << std::endl;
      std::cout << "no such file or directory " << std::endl;
    }

  }
  
  return ui.getSample();
}
