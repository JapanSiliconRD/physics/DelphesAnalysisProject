#!/bin/bash
MG5DIR="/home/kojin/work/Physics/inputfiles/MG5_aMC_v3_3_1"

if [ ! $1 ]
then
    echo "usage : $0 [rootfile]"
fi
ROOTFILE=$1
SAMPLENAME=`basename $ROOTFILE`
SAMPLENAME=${SAMPLENAME%.root}
SHORTNAME=${SAMPLENAME#emucollider_}
XSECSTR=`grep "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" $MG5DIR/${SAMPLENAME}/HTML/run_01/results.html  | head -1  | awk '{print $2"+-"$4"pb"}'`
XSECPB=`grep "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" $MG5DIR/${SAMPLENAME}/HTML/run_01/results.html  | head -1  | awk '{print $2}'`
GENSTR=`grep generate  $MG5DIR/${SAMPLENAME}/Cards/proc_card_mg5.dat`
GENSTR=${GENSTR#generate }


EventInab=`echo "$XSECPB * 1000 * 1000" | sed s/e+/\*10\^/g | sed s/e/\*10\^/g | bc -l` 
EventInab=`printf "%.3f" $EventInab`

echo $SHORTNAME" &  "$GENSTR"  &  "$XSECSTR"  & "$EventInab " \\\\ "


if [ ! $2 ]; then exit ;fi

if [ $2 != "ZBFHbbGenLevel" -a $2 != "ZBFHbbPIDcheck" -a $2 != "ZBFHbbAnalysis" -a $2 != "ZBFRecAnalysis" -a $2 != "WBFHbbAnalysis" ] 
then
    echo "jobtype $2 not defined.  please use right jobtype"
    exit
fi

cat > ${SAMPLENAME}_$2 <<EOF
jobtype $2
treename Delphes
from 0
to -1
input /home/kojin/work/Physics/delphout/${SAMPLENAME}.root
output results/results_${SAMPLENAME}_$2.root
execute
EOF

