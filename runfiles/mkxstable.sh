#! /bin/bash
TEXSRC="crosssection.tex"
cat > $TEXSRC <<EOF
\documentclass[10pt]{article} 
\usepackage[margin=10truemm]{geometry}
\begin{document}
\begin{center}
    \begin{tabular}{llll}
    \hline
    \hline
    Process & MG5 generation & Cross section [pb] & \# event in ab$^{-1}$ \\\\ \hline
    \hline
EOF

for ii in /home/kojin/work/Physics/delphout/emucollider_*.root 
do 
    ./mkrunfile.sh ${ii} 
done >> ${TEXSRC}_tmp

cat ${TEXSRC}_tmp | awk -F\& '{print $1" "$3" "$4}' > ${TEXSRC%.tex}.txt
cat ${TEXSRC}_tmp  | sed s/'_'/'\\_'/g | sed s/'\$'/'\\$'/g | sed s/'>'/'$>$'/g  >> $TEXSRC
rm -f  ${TEXSRC}_tmp

cat >> $TEXSRC <<EOF
    \hline
    \hline
    \end{tabular}
\end{center}
\end{document}
EOF

cat $TEXSRC
pdflatex $TEXSRC
rm -f ${TEXSRC%.tex}.aux ${TEXSRC%.tex}.log
