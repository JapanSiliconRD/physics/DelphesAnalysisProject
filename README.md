# Software package for Delphes simulation and analysis
##  Author Koji Nakamura <Koji.Nakamura@cern.ch>
####   first created on 18th Mar 2022


## [INSTALL]
### before install this software :
####  setup g++ >=4.8
####  setup ROOT (root 6 is recommended)
####  example at Normal CentOS7 PC with root installed by yum
    source /opt/rh/devtoolset-7/enable
    git clone https://gitlab.cern.ch/kojin/DelphesAnalysisProject.git
    cd DelphesAnalysisProject
    # if necessary checkout branch
    [e.g.  git checkout devel]
    cd config
    ./configure.sh
    source setup_local.sh

####  example at lxplus or lxatut
    git clone https://gitlab.cern.ch/kojin/LGADSoftware.git
    cd LGADSoftware
    # if necessary checkout branch
    [e.g.  git checkout devel]
    cd config
    source setupATLAS.sh
    ./configure.sh
    source setup_local.sh

####  configure.sh need only once when you install the package. next time you login :
    source setup_local.sh


## [COMPILE]
### use cmake to compile packages.
    mkdir builds
    cd builds 
    cmake3 ..
    make
    make install
    cd ../

## [Execute LGADanalysis]

    ./bin/DelphesAnalysis < runfiles/mue_ZBFH_ATLAS


