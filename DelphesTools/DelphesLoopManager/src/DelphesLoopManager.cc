#include "DelphesLoopManager.hh"

DelphesLoopManager::DelphesLoopManager(UserInterFace _ui){
  SetUserInterFace(_ui);
}
void DelphesLoopManager::Initialize(){
  std::cout <<  "initializing" << std::endl;
  initE.SetPxPyPzE(0,0,30,30);
  initMu.SetPxPyPzE(0,0,-1000,1000);
  initCMS=initE+initMu;
  Init(fChain);

  std::vector<std::string> categname; categname.clear();
  categname.push_back("All");

  TH1D* EMUZBFHbb_tmp= new TH1D("EMUZBFHbb_eventcounter",
				"EMUZBFHbb_eventcounter",
				20,-0.5,19.5);
  m_h_EMUZBFHbb_eventcounter.push_back(EMUZBFHbb_tmp);
  EMUZBFHbb_seltool = new SelectionTools(m_h_EMUZBFHbb_eventcounter,categname,10);
  EMUZBFHbb_seltool->SetName("All");

  TH1D* EMUWBFHbb_tmp= new TH1D("EMUWBFHbb_eventcounter",
				"EMUWBFHbb_eventcounter",
				20,-0.5,19.5);
  m_h_EMUWBFHbb_eventcounter.push_back(EMUWBFHbb_tmp);
  EMUWBFHbb_seltool = new SelectionTools(m_h_EMUWBFHbb_eventcounter,categname,11);
  EMUWBFHbb_seltool->SetName("All");

  TH1D* EMUZBFRec_tmp= new TH1D("EMUZBFRec_eventcounter",
				"EMUZBFRec_eventcounter",
				20,-0.5,19.5);
  m_h_EMUZBFRec_eventcounter.push_back(EMUZBFRec_tmp);
  EMUZBFRec_seltool = new SelectionTools(m_h_EMUZBFRec_eventcounter,categname,10);
  EMUZBFRec_seltool->SetName("All");

}
void DelphesLoopManager::Finalize(){
  if(ui.getJobType()=="ZBFHbbAnalysis") EMUZBFHbb_seltool->PrintAcceptanceTable();
  if(ui.getJobType()=="ZBFRecAnalysis") EMUZBFRec_seltool->PrintAcceptanceTable();
  if(ui.getJobType()=="WBFHbbAnalysis") EMUWBFHbb_seltool->PrintAcceptanceTable();
  
}

void DelphesLoopManager::Execute(){
  Initialize();
  Int_t nentries = Int_t(fChain->GetEntries());
  std::cerr << "Total Number of Events = "  << nentries << std::endl;
  int nLoopEvents; 
  if(ui.getEndEvent()==-1)nLoopEvents=nentries;
  else nLoopEvents=ui.getEndEvent();
  std::cout << "starting loop" << std::endl;
  for (Int_t jentry=0; jentry<nLoopEvents;jentry++) {
    if(jentry<ui.getStartEvent())continue;
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    fChain->GetEntry(jentry); 
    if(jentry%10000==0){
      std::cout << std::setw(7) << jentry << "th event process" << std::endl;
    }
    if(jentry<3)PrintParticle();
    Action();
  }
  Finalize();
}

void DelphesLoopManager::Action(){
  EMUZBFHbb_EventSelectionBit=0;
  EMUZBFRec_EventSelectionBit=0;
  EMUWBFHbb_EventSelectionBit=0;
  SetRecoParticle();
  if(ui.getJobType()=="ZBFHbbGenLevel") doZBFHbbGenLevelAnalysis();
  if(ui.getJobType()=="ZBFHbbPIDcheck"){
    doZBFHbbGenLevelAnalysis();
    doZBFHbbPIDcheckAnalysis();
  }
  if(ui.getJobType()=="ZBFHbbAnalysis"||ui.getJobType()=="ZBFRecAnalysis") doZBFGenEMUFinder();

  if(ui.getJobType()=="ZBFHbbAnalysis") doZBFHbbAnalysis();
  if(ui.getJobType()=="ZBFRecAnalysis") doZBFRecAnalysis();
  if(ui.getJobType()=="WBFHbbAnalysis") doWBFHbbAnalysis();
  FillHistogram();
}
void DelphesLoopManager::doZBFHbbAnalysis(){
  if(0)EMUZBFHbb_EventSelectionBit |= EventSelection::EMUZBFHbb_Trigger;
  if(m_Muon.size()==0) EMUZBFHbb_EventSelectionBit |= EventSelection::EMUZBFHbb_Muon;
  if(m_Elec.size()==0) EMUZBFHbb_EventSelectionBit |= EventSelection::EMUZBFHbb_Elec;
  if(m_Muon.size()>=2) EMUZBFHbb_EventSelectionBit |= EventSelection::EMUZBFHbb_DiMuonVeto;
  if(m_Elec.size()>=2) EMUZBFHbb_EventSelectionBit |= EventSelection::EMUZBFHbb_DiElecVeto;
  if(m_Jet.size()<2) EMUZBFHbb_EventSelectionBit |= EventSelection::EMUZBFHbb_Jets;
  if(m_BJet.size()<2) EMUZBFHbb_EventSelectionBit |= EventSelection::EMUZBFHbb_BJets;
  EMUZBFHbb_seltool->AddSelectionBit(EMUZBFHbb_EventSelectionBit,0);
}
void DelphesLoopManager::doZBFRecAnalysis(){
  if(0)EMUZBFRec_EventSelectionBit |= EventSelection::EMUZBFRec_Trigger;
  if(m_Muon.size()==0) EMUZBFRec_EventSelectionBit |= EventSelection::EMUZBFRec_Muon;
  if(m_Elec.size()==0) EMUZBFRec_EventSelectionBit |= EventSelection::EMUZBFRec_Elec;
  // add some selection to reject W events.

  EMUZBFRec_seltool->AddSelectionBit(EMUZBFRec_EventSelectionBit,0);
}
void DelphesLoopManager::doWBFHbbAnalysis(){
  if(0)EMUWBFHbb_EventSelectionBit |= EventSelection::EMUWBFHbb_Trigger;
  if(m_Muon.size()>0) EMUWBFHbb_EventSelectionBit |= EventSelection::EMUWBFHbb_NoMuon;
  if(m_Elec.size()>0) EMUWBFHbb_EventSelectionBit |= EventSelection::EMUWBFHbb_NoElec;
  if(m_Jet.size()<2) EMUWBFHbb_EventSelectionBit |= EventSelection::EMUWBFHbb_Jets;
  if(m_BJet.size()<2) EMUWBFHbb_EventSelectionBit |= EventSelection::EMUWBFHbb_BJets;
  EMUWBFHbb_seltool->AddSelectionBit(EMUWBFHbb_EventSelectionBit,0);
}
void DelphesLoopManager::doZBFHbbPIDcheckAnalysis(){
  // To be implemented truth matching study.
  
}
void DelphesLoopManager::doZBFGenEMUFinder(){
  bool foundmu=false;
  bool founde=false;

  for(int ipar=0;ipar<Particle_size;ipar++){
    TLorentzVector lv;
    lv.SetPxPyPzE(Particle_Px[ipar],Particle_Py[ipar],Particle_Pz[ipar],Particle_E[ipar]);
    if(foundmu==false&&(Particle_Status[ipar]==2||Particle_Status[ipar]==1) &&Particle_PID[ipar]==-13){
      m_ParFinalMuon= lv;
      foundmu=true;
    }
    if(founde==false&&(Particle_Status[ipar]==2 ||Particle_Status[ipar]==1) &&Particle_PID[ipar]==11){
      m_ParFinalElec= lv;
      founde=true;
    }
    if(founde&&foundmu)break;
  }
}

void DelphesLoopManager::doZBFHbbGenLevelAnalysis(){
  m_nParBeamMuon=0;
  m_nParBeamElec=0;
  m_nParFinalMuon=0;
  m_nParFinalElec=0;
  m_nParHiggs=0;
  m_nParBquark1=0;
  m_nParBquark2=0;
  int iparHD1=-1;
  int iparHD2=-1;
  for(int ipar=0;ipar<Particle_size;ipar++){
    TLorentzVector lv;
    lv.SetPxPyPzE(Particle_Px[ipar],Particle_Py[ipar],Particle_Pz[ipar],Particle_E[ipar]);
    if(ipar==4 || ipar==5){
      if(Particle_PID[ipar]==-13){
	m_ParBeamMuon =lv;
	m_nParBeamMuon++;
      }
      if(Particle_PID[ipar]==11){
	m_ParBeamElec =lv;
	m_nParBeamElec++;
      }
    }
    if(Particle_Status[ipar]==2 && Particle_PID[ipar]==25) {
      m_ParHiggs =lv;
      m_nParHiggs++;
      iparHD1=Particle_D1[ipar];
      iparHD2=Particle_D2[ipar];
    }
    if(Particle_Status[ipar]!=3 && Particle_PID[ipar]==-13  && Particle_Status[Particle_M1[ipar]]==3 && Particle_PID[Particle_M1[ipar]]==-13) {
      int jpar=ipar;
      while(Particle_D1[jpar]!=-1){
	jpar=Particle_D1[jpar];
      }
      lv.SetPxPyPzE(Particle_Px[jpar],Particle_Py[jpar],Particle_Pz[jpar],Particle_E[jpar]);
      m_ParFinalMuon= lv;
      m_nParFinalMuon++;
    }
    if(Particle_Status[ipar]!=3 && Particle_PID[ipar]==11 && Particle_Status[Particle_M1[ipar]]==3 && Particle_PID[Particle_M1[ipar]]==11){
      int jpar=ipar;
      while(Particle_D1[jpar]!=-1){
	jpar=Particle_D1[jpar];
      }
      lv.SetPxPyPzE(Particle_Px[jpar],Particle_Py[jpar],Particle_Pz[jpar],Particle_E[jpar]);
      m_ParFinalElec =lv;
      m_nParFinalElec++;
    }
    if(ipar>=iparHD1&&ipar<=iparHD2){
      if(Particle_PID[ipar]==5){
	m_ParBquark1 =lv;
	m_nParBquark1++;
      }
      if(Particle_PID[ipar]==-5){
	m_ParBquark2 =lv;
	m_nParBquark2++;
      }
    }
  }
  if(m_nParFinalMuon!=1 || m_nParFinalElec!=1 || m_nParHiggs!=1){
    std::cout << m_nParBeamMuon << " " 
	      << m_nParBeamElec << " " 
	      << m_nParFinalMuon << " " 
	      << m_nParFinalElec << " " 
	      << m_nParHiggs << " " 
	      << m_nParBquark1 << " " 
	      << m_nParBquark2 << std::endl;
    PrintParticle();
  }
}

void DelphesLoopManager::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  hvs->Add4vecHistogram("gen_BeamMuon",200,0,200);
  hvs->Add4vecHistogram("gen_BeamElec",200,0,200);
  hvs->Add4vecHistogram("gen_FinalMuon",200,0,200);
  hvs->Add4vecHistogram("gen_FinalElec",200,0,200);
  hvs->Add4vecHistogram("gen_Higgs",200,0,200);
  hvs->Add4vecHistogram("gen_Bquark1",200,0,200);
  hvs->Add4vecHistogram("gen_Bquark2",200,0,200);
  hvs->Add4vecHistogram("gen_bbsystem",200,0,200);
  hvs->Add2DHistogram("gen_MuonPtvsTheta",100,0,100,100,0,20);
  hvs->Add4vecHistogram("rec_Muon",200,0,200);
  hvs->Add4vecHistogram("rec_Elec",200,0,200);
  hvs->Add4vecHistogram("rec_Jet1",200,0,200);
  hvs->Add4vecHistogram("rec_Jet2",200,0,200);
  hvs->Add4vecHistogram("rec_Jet3",200,0,200);
  hvs->Add4vecHistogram("rec_Jet4",200,0,200);
  hvs->Add4vecHistogram("rec_BJet1",200,0,200);
  hvs->Add4vecHistogram("rec_BJet2",200,0,200);
  hvs->Add4vecHistogram("rec_BJet3",200,0,200);
  hvs->Add4vecHistogram("rec_BJet4",200,0,200);

  hvs->Add4vecHistogram("recSel_Muon",200,0,200);
  hvs->Add4vecHistogram("recSel_Elec",200,0,200);
  hvs->Add4vecHistogram("recSel_Jet1",200,0,200);
  hvs->Add4vecHistogram("recSel_Jet2",200,0,200);
  hvs->Add4vecHistogram("recSel_Jet3",200,0,200);
  hvs->Add4vecHistogram("recSel_Jet4",200,0,200);
  hvs->Add4vecHistogram("recSel_BJet1",200,0,200);
  hvs->Add4vecHistogram("recSel_BJet2",200,0,200);
  hvs->Add4vecHistogram("recSel_BJet3",200,0,200);
  hvs->Add4vecHistogram("recSel_BJet4",200,0,200);
  hvs->Add4vecHistogram("recSel_bbsystem",200,0,200);
  hvs->Add4vecHistogram("TrueRecoil",300,0,300);
  hvs->Add4vecHistogram("MaxERecoil",300,0,300);
  hvs->Add4vecHistogram("MaxEtaRecoil",300,0,300);
}
void DelphesLoopManager::FillHistogram(){
  if(ui.getJobType()=="ZBFHbbGenLevel" ||ui.getJobType()=="ZBFHbbPIDcheck"){
    Fill4VecHistogram(hvs,"gen_BeamElec",m_ParBeamElec);
    Fill4VecHistogram(hvs,"gen_FinalMuon",m_ParFinalMuon);
    Fill4VecHistogram(hvs,"gen_FinalElec",m_ParFinalElec);
    Fill4VecHistogram(hvs,"gen_Higgs",m_ParHiggs);
    if(m_nParBquark1)Fill4VecHistogram(hvs,"gen_Bquark1",m_ParBquark1);
    if(m_nParBquark2)Fill4VecHistogram(hvs,"gen_Bquark2",m_ParBquark2);
    if(m_nParBquark1&&m_nParBquark2)Fill4VecHistogram(hvs,"gen_bbsystem",m_ParBquark1+m_ParBquark2);
    hvs->Get2DHistogram("gen_MuonPtvsTheta")->Fill(m_ParFinalMuon.Pt(),180-m_ParFinalMuon.Theta()*TMath::RadToDeg());
    if(m_Muon.size()>0) Fill4VecHistogram(hvs,"rec_Muon",m_Muon[0].P4());
    if(m_Elec.size()>0) Fill4VecHistogram(hvs,"rec_Elec",m_Elec[0].P4());
    if(m_Jet.size()>0) Fill4VecHistogram(hvs,"rec_Jet1",m_Jet[0].P4());
    if(m_Jet.size()>1) Fill4VecHistogram(hvs,"rec_Jet2",m_Jet[1].P4());
    if(m_Jet.size()>2) Fill4VecHistogram(hvs,"rec_Jet3",m_Jet[2].P4());
    if(m_Jet.size()>3) Fill4VecHistogram(hvs,"rec_Jet4",m_Jet[3].P4());
    if(m_Jet.size()>0) if(m_Jet[0].BTag==1)Fill4VecHistogram(hvs,"rec_BJet1",m_Jet[0].P4());
    if(m_Jet.size()>1) if(m_Jet[1].BTag==1)Fill4VecHistogram(hvs,"rec_BJet2",m_Jet[1].P4());
    if(m_Jet.size()>2) if(m_Jet[2].BTag==1)Fill4VecHistogram(hvs,"rec_BJet3",m_Jet[2].P4());
    if(m_Jet.size()>3) if(m_Jet[3].BTag==1)Fill4VecHistogram(hvs,"rec_BJet4",m_Jet[3].P4());
  }
  
  if(ui.getJobType()=="ZBFHbbAnalysis" && EMUZBFHbb_EventSelectionBit==0
     || ui.getJobType()=="WBFHbbAnalysis" && EMUWBFHbb_EventSelectionBit==0){
    if(m_Muon.size()>0) Fill4VecHistogram(hvs,"recSel_Muon",m_Muon[0].P4());
    if(m_Elec.size()>0) Fill4VecHistogram(hvs,"recSel_Elec",m_Elec[0].P4());
    if(m_Jet.size()>0) Fill4VecHistogram(hvs,"recSel_Jet1",m_Jet[0].P4());
    if(m_Jet.size()>1) Fill4VecHistogram(hvs,"recSel_Jet2",m_Jet[1].P4());
    if(m_Jet.size()>2) Fill4VecHistogram(hvs,"recSel_Jet3",m_Jet[2].P4());
    if(m_Jet.size()>3) Fill4VecHistogram(hvs,"recSel_Jet4",m_Jet[3].P4());
    if(m_BJet.size()>0)Fill4VecHistogram(hvs,"recSel_BJet1",m_BJet[0].P4());
    if(m_BJet.size()>1)Fill4VecHistogram(hvs,"recSel_BJet2",m_BJet[1].P4());
    if(m_BJet.size()>2)Fill4VecHistogram(hvs,"recSel_BJet3",m_BJet[2].P4());
    if(m_BJet.size()>3)Fill4VecHistogram(hvs,"recSel_BJet4",m_BJet[3].P4());
    if(m_BJet.size()>1)Fill4VecHistogram(hvs,"recSel_bbsystem",m_BJet[0].P4()+m_BJet[1].P4());
  }
  if(ui.getJobType()=="ZBFRecAnalysis" && EMUZBFRec_EventSelectionBit==0){
    // identify ZBF muon and electron 
    // assume m_Muon[0] and m_Elec[0] are ZBF leptons.
    if(0){
      if(m_Muon.size()>1){
	std::cout << "True Mu (E,phi)=(" << m_ParFinalMuon.E() <<"," << m_ParFinalMuon.Phi() << ") : ";
	for(int imu=0;imu<m_Muon.size();imu++){
	  std::cout << "(" <<  m_Muon[imu].P4().E() << "," << m_Muon[imu].P4().Phi() << ") " ;
	}
	std::cout << std::endl;
      }
    }
    
    double minmudR=100000;
    double murE=0;
    int mudRindex=-1;
    double maxmuE=0;
    int mumaxEind=-1;
    double maxmuEta=0;
    int mumaxEtaind=-1;
    for(int imu=0;imu<m_Muon.size();imu++){
      double dR=m_Muon[imu].P4().DeltaR(m_ParFinalMuon);
      double rE=m_Muon[imu].P4().E()/m_ParFinalMuon.E();
      //      std::cout << "mu : " << dR << " " << rE << std::endl;
      if(minmudR>dR){
	minmudR=dR;
	mudRindex=imu;
	murE=rE;
      }
      if(maxmuE<m_Muon[imu].P4().E()){
	maxmuE=m_Muon[imu].P4().E();
	mumaxEind=imu;
      }
      if(maxmuEta<fabs(m_Muon[imu].P4().Eta())){
	maxmuEta=fabs(m_Muon[imu].P4().Eta());
	mumaxEtaind=imu;
      }
    }
    double minedR=100000;
    double erE=0;
    int edRindex=-1;
    double maxeE=0;
    int emaxEind=-1;
    double maxeEta=0;
    int emaxEtaind=-1;
    for(int ie=0;ie<m_Elec.size();ie++){
      double dR=m_Elec[ie].P4().DeltaR(m_ParFinalElec);
      double rE=m_Elec[ie].P4().E()/m_ParFinalElec.E();
      //      std::cout << "e : " << dR << " " << rE << std::endl;
      if(minedR>dR){
	minedR=dR;
	edRindex=ie;
	erE=rE;
      }
      if(maxeE<m_Elec[ie].P4().E()){
	maxeE=m_Elec[ie].P4().E();
	emaxEind=ie;
      }
      if(maxeEta<fabs(m_Elec[ie].P4().Eta())){
	maxeEta=fabs(m_Elec[ie].P4().Eta());
	emaxEtaind=ie;
      }
    }
    if(minmudR<0.4&&fabs(murE-1)<0.1&&minedR<0.4&&fabs(erE-1)<0.1){
      Fill4VecHistogram(hvs,"TrueRecoil",initCMS-(m_Elec[edRindex].P4()+m_Muon[mudRindex].P4()));
    }else{
      //      std::cout <<  minmudR << " " << fabs(murE-1) << " " << minedR << " " << fabs(erE-1) << std::endl;
    }
    Fill4VecHistogram(hvs,"MaxERecoil",initCMS-(m_Elec[emaxEind].P4()+m_Muon[mumaxEind].P4()));
    Fill4VecHistogram(hvs,"MaxEtaRecoil",initCMS-(m_Elec[emaxEtaind].P4()+m_Muon[mumaxEtaind].P4()));
  }
     
}



void DelphesLoopManager::SetRecoParticle(){
  m_Muon.clear();
  m_Elec.clear();
  m_Jet.clear();
  m_BJet.clear();
  //  std::cout <<Muon_size << std::endl;
  for(int imu=0;imu<Muon_size;imu++){
    Muon mu;
    mu.PT=Muon_PT[imu];
    mu.Eta=Muon_Eta[imu];
    mu.Phi=Muon_Phi[imu];
    mu.T=Muon_T[imu];
    mu.Charge=Muon_Charge[imu];
    //    mu.Particle =Muon_Particle[imu];
    mu.IsolationVar =Muon_IsolationVar[imu];
    mu.IsolationVarRhoCorr =Muon_IsolationVarRhoCorr[imu];
    mu.SumPtCharged =Muon_SumPtCharged[imu];
    mu.SumPtNeutral =Muon_SumPtNeutral[imu];
    mu.SumPtChargedPU =Muon_SumPtChargedPU[imu];
    mu.SumPt =Muon_SumPt[imu];
    mu.D0 =Muon_D0[imu];
    mu.DZ =Muon_DZ[imu];
    mu.ErrorD0 =Muon_ErrorD0[imu];
    mu.ErrorDZ =Muon_ErrorDZ[imu];
    //    std::cout << mu.PT << " " << mu.P4().Pt() << std::endl;
    m_Muon.push_back(mu);
  }
  for(int iele=0;iele<Electron_size;iele++){
    Electron ele;
    ele.PT=Electron_PT[iele];
    ele.Eta=Electron_Eta[iele];
    ele.Phi=Electron_Phi[iele];
    ele.T=Electron_T[iele];
    ele.Charge=Electron_Charge[iele];
    //    ele.Particle =Electron_Particle[iele];
    ele.IsolationVar =Electron_IsolationVar[iele];
    ele.IsolationVarRhoCorr =Electron_IsolationVarRhoCorr[iele];
    ele.SumPtCharged =Electron_SumPtCharged[iele];
    ele.SumPtNeutral =Electron_SumPtNeutral[iele];
    ele.SumPtChargedPU =Electron_SumPtChargedPU[iele];
    ele.SumPt =Electron_SumPt[iele];
    ele.D0 =Electron_D0[iele];
    ele.DZ =Electron_DZ[iele];
    ele.ErrorD0 =Electron_ErrorD0[iele];
    ele.ErrorDZ =Electron_ErrorDZ[iele];
    //    std::cout << mu.PT << " " << mu.P4().Pt() << std::endl;
    m_Elec.push_back(ele);
  }
  for(int ijet=0;ijet<Jet_size;ijet++){
    Jet jet;
    jet.PT =Jet_PT[ijet];
    jet.Eta =Jet_Eta[ijet];
    jet.Phi =Jet_Phi[ijet];
    jet.T =Jet_T[ijet];
    jet.Mass =Jet_Mass[ijet];
    jet.DeltaEta =Jet_DeltaEta[ijet];
    jet.DeltaPhi =Jet_DeltaPhi[ijet];
    jet.Flavor =Jet_Flavor[ijet];
    jet.FlavorAlgo =Jet_FlavorAlgo[ijet];
    jet.FlavorPhys =Jet_FlavorPhys[ijet];
    jet.BTag =Jet_BTag[ijet];
    jet.BTagAlgo =Jet_BTagAlgo[ijet];
    jet.BTagPhys =Jet_BTagPhys[ijet];
    jet.TauTag =Jet_TauTag[ijet];
    jet.TauWeight =Jet_TauWeight[ijet];
    jet.Charge =Jet_Charge[ijet];
    jet.EhadOverEem =Jet_EhadOverEem[ijet];
    jet.NCharged =Jet_NCharged[ijet];
    jet.NNeutrals =Jet_NNeutrals[ijet];
    jet.NeutralEnergyFraction  =Jet_NeutralEnergyFraction[ijet];
    jet.ChargedEnergyFraction =Jet_ChargedEnergyFraction[ijet];
    jet.Beta =Jet_Beta[ijet];
    jet.BetaStar =Jet_BetaStar[ijet];
    jet.MeanSqDeltaR =Jet_MeanSqDeltaR[ijet];
    jet.PTD =Jet_PTD[ijet];
    jet.SoftDroppedJet =Jet_SoftDroppedJet[ijet];
    jet.SoftDroppedSubJet1 =Jet_SoftDroppedSubJet1[ijet];
    jet.SoftDroppedSubJet2 =Jet_SoftDroppedSubJet2[ijet];
    for(int ii=0;ii<5;ii++){
      jet.FracPt[ii] =Jet_FracPt[ijet][ii];
      jet.Tau[ii] =Jet_Tau[ijet][ii];
      jet.TrimmedP4[ii] =Jet_TrimmedP4[ii][ijet];
      jet.PrunedP4[ii] =Jet_PrunedP4[ii][ijet];
      jet.SoftDroppedP4[ii] =Jet_SoftDroppedP4[ii][ijet];
    }
    jet.NSubJetsTrimmed =Jet_NSubJetsTrimmed[ijet];
    jet.NSubJetsPruned =Jet_NSubJetsPruned[ijet];
    jet.NSubJetsSoftDropped =Jet_NSubJetsSoftDropped[ijet];
    jet.ExclYmerge23 =Jet_ExclYmerge23[ijet];
    jet.ExclYmerge34 =Jet_ExclYmerge34[ijet];
    jet.ExclYmerge45 =Jet_ExclYmerge45[ijet];
    jet.ExclYmerge56 =Jet_ExclYmerge56[ijet];
    jet.Constituents =Jet_Constituents[ijet];
    //    jet.Particles =Jet_Particles[ijet];
    jet.Area =Jet_Area[ijet];
    m_Jet.push_back(jet);
  }
  for(auto x : m_Jet){
    if(x.BTag==1)m_BJet.push_back(x);
  }
  std::sort(m_Elec.begin(),m_Elec.end());
  std::sort(m_Muon.begin(),m_Muon.end());
  std::sort(m_Jet.begin(),m_Jet.end());

}

void DelphesLoopManager::Fill4VecHistogram(HistogramVariableStrage *_hvs,std::string name, TLorentzVector lv,double weight){
  _hvs->Get4vecHistogram(name)[0]->Fill(lv.Pt(),weight);
  _hvs->Get4vecHistogram(name)[1]->Fill(lv.Eta(),weight);
  _hvs->Get4vecHistogram(name)[2]->Fill(lv.Phi(),weight);
  _hvs->Get4vecHistogram(name)[3]->Fill(lv.M(),weight);
  _hvs->Get4vecHistogram(name)[4]->Fill(180-lv.Theta()*TMath::RadToDeg(),weight);
}

void DelphesLoopManager::PrintParticle(){
  std::cout << "==============================================" << std::endl;
  std::cout << Particle_size << std::endl;
  std::cout << std::setw(7) << "Number" << " "
	    << std::setw(5) << "PID" << " " 
	    << std::setw(5) << "Status" << " "
	    << std::setw(5) << "M1" << " "
	    << std::setw(5) << "M2" << " "
	    << std::setw(5) << "D1" << " "
	    << std::setw(5) << "D2" << " "
	    << std::setw(10) << "PT" << " "
	    << std::setw(10) << "Eta" << " " 
	    << std::setw(10) << "Phi" << " " 
	    << std::setw(10) << "Mass" << " " 
	    << std::endl;
  for(int ipar=0;ipar<Particle_size;ipar++){
    std::cout << std::setw(7) << ipar << " "
	      << std::setw(5) << Particle_PID[ipar] << " " 
	      << std::setw(5) << Particle_Status[ipar] << " "
	      << std::setw(5) << Particle_M1[ipar] << " "
	      << std::setw(5) << Particle_M2[ipar] << " "
	      << std::setw(5) << Particle_D1[ipar] << " "
	      << std::setw(5) << Particle_D2[ipar] << " "
	      << std::setw(10) << Particle_PT[ipar] << " "
	      << std::setw(10) << Particle_Eta[ipar] << " " 
	      << std::setw(10) << Particle_Phi[ipar] << " " 
	      << std::setw(10) << Particle_Mass[ipar] << " " 
	      << std::endl;
  }
}
