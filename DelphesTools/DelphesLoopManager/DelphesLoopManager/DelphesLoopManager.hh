#ifndef __DelphesLoopManager_hh__
#define __DelphesLoopManager_hh__
#include "DelphesTreeInfo.h"
#include "TLorentzVector.h"
#include "UserInterFace.hh"
#include "HistogramVariableStrage.hh"
#include "TMath.h"
#include "DelphesClasses.h"
#include "SelectionTools.h"
#ifdef __CLING__
R__LOAD_LIBRARY(libDelphes)
#include "classes/DelphesClasses.h"
#include "external/ExRootAnalysis/ExRootTreeReader.h"
#include "external/ExRootAnalysis/ExRootResult.h"
#else
class ExRootTreeReader;
class ExRootResult;
#endif



class DelphesLoopManager : public DelphesTreeInfo {
private:
  UserInterFace ui;
  HistogramVariableStrage *hvs;
  
  TLorentzVector initE;
  TLorentzVector initMu;
  TLorentzVector initCMS;

  int EMUZBFHbb_EventSelectionBit;
  int EMUWBFHbb_EventSelectionBit;
  int EMUZBFRec_EventSelectionBit;

  TLorentzVector  m_ParBeamMuon;
  TLorentzVector  m_ParBeamElec;
  TLorentzVector  m_ParFinalMuon;
  TLorentzVector  m_ParFinalElec;
  TLorentzVector  m_ParHiggs;
  TLorentzVector  m_ParBquark1;
  TLorentzVector  m_ParBquark2;
  int m_nParBeamMuon;
  int m_nParBeamElec;
  int m_nParFinalMuon;
  int m_nParFinalElec;
  int m_nParHiggs;
  int m_nParBquark1;
  int m_nParBquark2;
  std::vector<Muon> m_Muon;
  std::vector<Electron> m_Elec;
  std::vector<Jet> m_Jet;
  std::vector<Jet> m_BJet;

  std::vector<TH1D*>m_h_EMUZBFHbb_eventcounter;
  std::vector<TH1D*>m_h_EMUZBFRec_eventcounter;
  std::vector<TH1D*>m_h_EMUWBFHbb_eventcounter;
  SelectionTools *EMUZBFHbb_seltool;
  SelectionTools *EMUZBFRec_seltool;
  SelectionTools *EMUWBFHbb_seltool;

  void PrintParticle();
  void SetRecoParticle();
  void doWBFHbbAnalysis();
  void doZBFHbbAnalysis();
  void doZBFRecAnalysis();
  void doZBFGenEMUFinder();
  void doZBFHbbGenLevelAnalysis();
  void doZBFHbbPIDcheckAnalysis();
  void FillHistogram();
  void Fill4VecHistogram(HistogramVariableStrage *_hvs,std::string name, TLorentzVector lv,double weight=1);
public:
  DelphesLoopManager(){};
  DelphesLoopManager(UserInterFace _ui);
  ~DelphesLoopManager(){};
  void Initialize();
  void Finalize();
  void Action();
  void Execute();
  void SetUserInterFace(UserInterFace _ui){ui=_ui;}
  void SetChain(TTree *_chain){fChain=_chain;}
  void SetHistogram(HistogramVariableStrage *hvs);
  


};


#endif
