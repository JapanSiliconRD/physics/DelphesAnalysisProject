#! /bin/bash
TYPE="DelphesTreeInfo"
filename=/home/kojin/work/Physics/muTristan/delphout/emucollider_ZBFHbb60GeV500GeV_v1.root
TREENAME="Delphes"

#mv ${TYPE}.h ${TYPE}.h_orig
#mv ${TYPE}.C ${TYPE}.C_orig
root -l -q -b "mkTreeInfo.C(\"$filename\",\"$TYPE\",\"$TREENAME\")"

sed s/vector\</std::vector\</g ${TYPE}.h > ${TYPE}.h_tmp
rm -f ${TYPE}.h
sed s/string/std::string/g ${TYPE}.h_tmp > ${TYPE}.h
rm -f ${TYPE}.h_tmp

#touch ${TYPE}.h 
#while read line
#  do
#  echo "${line}" >> ${TYPE}.h 
#  if [ "$line" ] 
#      then
#      if [ "$line" == 'virtual void     Show(Long64_t entry = -1);' ]
#	  then
#	  echo "   virtual void   Initialize(){}" >> ${TYPE}.h 
#	  echo "   virtual void   Finalize(){}" >> ${TYPE}.h 
#	  echo "   virtual void   Action(){}" >> ${TYPE}.h 
#      fi
#  fi
#done < ${TYPE}.h_tmp

sed '/virtual ~DelphesTreeInfo/a \   virtual void     Initialize(){}\n   virtual void     Finalize(){}\n   virtual void     Action(){}'  ${TYPE}.h > ${TYPE}.h_tmp
rm -f ${TYPE}.h ; mv ${TYPE}.h_tmp ${TYPE}.h
    
sed '/#include <TFile.h>/a \#include "TLorentzVector.h"\n#include "TRef.h"\n#include "TRefArray.h"\n\n#ifdef __CLING__\nR__LOAD_LIBRARY(libDelphes)\n#include "classes/DelphesClasses.h"\n#include "external/ExRootAnalysis/ExRootTreeReader.h"\n#include "external/ExRootAnalysis/ExRootResult.h"\n#else\nclass ExRootTreeReader;\nclass ExRootResult;\n#endif' ${TYPE}.h  > ${TYPE}.h_tmp
rm -f ${TYPE}.h ; mv ${TYPE}.h_tmp ${TYPE}.h










mv ${TYPE}.C ../src/${TYPE}.cc
exit;


#add
#include <vector>
#include <TClonesArray.h>

#add

   virtual void     Finalize(){}
   virtual void     Action(){}
